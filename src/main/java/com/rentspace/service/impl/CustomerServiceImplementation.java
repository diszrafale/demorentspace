package com.rentspace.service.impl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rentspace.domain.Customer;
import com.rentspace.dto.RegisterDto;
import com.rentspace.exception.UnknownException;
import com.rentspace.repository.CustomerRepository;
import com.rentspace.service.CustomerService;
@Service
public class CustomerServiceImplementation implements CustomerService{
	@Autowired
	private CustomerRepository customerRepository;
	@Override
	public Customer register(RegisterDto regDto) throws UnknownException {
		
		Customer customer = customerRepository.findByEmailAndPassword(regDto.getEmail(), regDto.getPassword());
		if (customer !=null){
			return customer;
		}
		throw new UnknownException(); 

	}
}
