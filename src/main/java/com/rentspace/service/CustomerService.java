package com.rentspace.service;

import com.rentspace.domain.Customer;
import com.rentspace.dto.RegisterDto;
import com.rentspace.exception.UnknownException;

public interface CustomerService  {
	public Customer register(RegisterDto regDto) throws UnknownException;
}
