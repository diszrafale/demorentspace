package com.rentspace.exception;

public class UnknownException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3737009584625061873L;
	
	private static final String DEFAULT_MESAGE = "User not found !";

	public UnknownException() {
		super(DEFAULT_MESAGE);
	}

	public UnknownException(String message) {
		super(message);
	}
	
	

}
