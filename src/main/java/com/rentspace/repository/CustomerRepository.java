package com.rentspace.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.rentspace.domain.Customer;

public interface CustomerRepository extends MongoRepository<Customer, String>{

	public Customer findByEmailAndPassword(String email, String password);
	}


