package com.rentspace;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoRentSpaceApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoRentSpaceApplication.class, args);
	}
}
