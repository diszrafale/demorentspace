package com.rentspace.controller;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.rentspace.domain.Customer;
import com.rentspace.dto.RegisterDto;
import com.rentspace.service.CustomerService;
import com.rentspace.util.RestResponse;
import com.rentspace.util.RestUtil;
@RestController
@RequestMapping(value = "customer")
public class CustomerController {
	@Autowired
	private CustomerService customerService;

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<RestResponse<Customer>> register(@RequestBody @Valid RegisterDto regDto) {
		return RestUtil.successResponse(customerService.register(regDto));
		
	}
}
