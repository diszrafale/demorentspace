package com.rentspace.domain;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
@Document
public class Customer extends AbstractMongo{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@NotNull
	@NotBlank
	@Indexed
	private String name;
	@NotNull
	@NotBlank
	@Indexed
	private String email;
	@NotNull
	@NotBlank
	@Indexed
	private String password;
	@NotNull
	@NotBlank
	@Indexed
	private String phone;
	public Customer() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Customer(String name, String email, String password, String phone) {
		super();
		this.name = name;
		this.email = email;
		this.password = password;
		this.phone = phone;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	
}
