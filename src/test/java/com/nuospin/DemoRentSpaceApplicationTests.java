package com.nuospin;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.rentspace.DemoRentSpaceApplication;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = DemoRentSpaceApplication.class)
@WebAppConfiguration
public class DemoRentSpaceApplicationTests {

	@Test
	public void contextLoads() {
	}

}
